package com.kdmlog.msg

sealed trait ErrorMessage { def msg: String }

class NoResult(value: String) extends ErrorMessage {
  def msg = s"Sorry, No Result Found. Please Try Another Keywords. Your Keywords: ${value}"
}

class NotFound(value: String) extends ErrorMessage {
  def msg = s"Oops! Not found page ${value}! You might have come wrong page or the page have been deleted."
}

class NotAllowed(value: String) extends ErrorMessage {
  def msg = "Method Not Allowed."
}

class InternalError(value: String) extends ErrorMessage {
  def msg = "Aww... X( Something Went Wrong."
}

class Unknown(value: String) extends ErrorMessage {
  def msg = "Somthing went wrong."
}

object Parser {
  def parseErrorCode(code: Int, value: String): ErrorMessage = {
    code match {
      case 0 => new NoResult(value)
      case 404 => new NotFound(value)
      case 405 => new NotAllowed("")
      case 500 => new InternalError("")
      case _ => new Unknown("")
    } 
  }
}
