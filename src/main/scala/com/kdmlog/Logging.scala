package com.kdmlog

import org.joda.time.DateTime

object Logging {
  final val DATE_FMT = "yyyy-MM-dd HH:mm:ss"

  def info(msg: String): Unit = {
    println(s"[${new DateTime()toString(DATE_FMT)}]  INFO: ${msg}")
  }
}
