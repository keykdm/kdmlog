package com.kdmlog

import org.joda.time.DateTime
import org.joda.time.format._
import java.sql.{Connection, DriverManager, Statement}
import scala.util.{Try, Success, Failure}

import com.kdmlog.dbentity._
import com.kdmlog.factory.DBEntityFactory
import com.kdmlog.factory.DBEntityFactory._
import com.kdmlog.parse.MarkDownParser._

object DBAccessor {
  
  final val URL = s"jdbc:mariadb://${sys.env("MYSQL_URL")}"
  final val DRIVER = "org.mariadb.jdbc.Driver"
  final val USER = sys.env("DB_USER") 
  final val PW = sys.env("DB_PW") 
  final val DB_TAGS = "tagmap"
  final val DB_COMMENTS = "comments"
  final val DB_POSTS = "posts"

  def getCon(): Connection = {
    Class.forName(DRIVER)
    DriverManager.getConnection(URL, USER, PW)
  } 

  def getTags(key: Int): Seq[Tag] = select[Tag](s"select post_id, name from ${DB_TAGS} where post_id = ${key}").getOrElse(Seq())
  def getComments(key: Int): Seq[Comment] = select[Comment](s"select post_id, name, body, comment_date from ${DB_COMMENTS} where post_id = ${key} order by comment_date desc").getOrElse(Seq())
  def getImage(id: Int): Option[Image] = select[Image](s"select * from images where image_id = ${id}").getOrElse(Seq()).headOption
  def parseTags(tagStr: String): Seq[String] = "[\\s\\,\\.]+".r.replaceAllIn(tagStr, " ").split(" ").toSeq
  
  def getLastId(state: Statement): Int = {
    val rs = state.executeQuery("select LAST_INSERT_ID()")
    if (rs.first()) rs.getInt("LAST_INSERT_ID()") 
    else 0
  }
  
  def postComment(postId: String, vals: (String, String, String)): Try[Unit] = Try {
    val state = getCon().createStatement
    val fields = "post_id, name, body, comment_date" 
    val name = escape(vals._1)
    val body = escape(vals._3)
    val now = new DateTime().toString(DATE_FMT)
    val q = s"insert into ${DB_COMMENTS}(${fields}) values('${postId}', '${name}', '${body}', '${now}')"
    state.executeUpdate(q)
  } 

  def updateArticle(id: Int, title: String, tags: String, body: String): Try[Unit] = Try {

  }

  def postArticle(title: String, tags: String, body: String): Try[Int] = Try {
    def findNewImages(str: String): Seq[(String, String)] = {
      def removeStr(list: Seq[String], target: String): String = {
        list match { case Seq() => target case x :: xs => removeStr(xs, target.replaceAll(x, "")) }
      }
      def isUrl(str: String): Boolean = str.matches("^https?.{0,}$")
      def getImageName(str: String): String = removeStr(Seq("^.{0,}\\!\\[", "\\].{0,}$"), str)
      def getImageUrl(str: String): String = removeStr(Seq("^.{0,}\\(", "\\).{0,}$"), str)

      val mkdwns = """!\[.+\]\(.+\)""".r.findAllMatchIn(str).map(m => m.group(0))
      val mkdwnTuples = mkdwns.map(m => (getImageName(m), getImageUrl(m)))
      mkdwnTuples.filter(x => isUrl(x._2)).toSeq.distinct
    }

    def replaceToNewImages(body: String, newSeq: Seq[(String, Int)]): String = {
      var newBody = body
      for (i <- newSeq) { newBody = newBody.replaceAll(s"\\!\\[${i._1}\\]\\(.{0,}\\)", s"\\!\\[${i._1}\\]\\(${i._2}\\)") }
      newBody
    }

    val con = getCon() 
    con.setAutoCommit(false)
    val state = con.createStatement

    val newBody = replaceToNewImages(body, findNewImages(body).map { i =>
      state.executeUpdate(s"insert into images(name, url) values('${i._1}', '${i._2}')")
      (i._1, getLastId(state))
    })
    val now = new DateTime().toString(DATE_FMT)
    state.executeUpdate(s"insert into ${DB_POSTS}(title, body, publish_date, update_date, likes) values('${escape(title)}', '${escape(newBody)}', '${now}', '${now}', 0)")
    getLastId(state) match {
      case 0 => {
        con.rollback()
        0
      }
      case x => {
        for (tag <- parseTags(tags)) state.executeUpdate(s"insert into ${DB_TAGS}(post_id, name) values(${x}, '${tag}')")
        con.commit()
        x
      }
    }
  }

  def escape(str: String): String = {
    str.replaceAll("\\\\", "\\\\\\\\").replaceAll("[\']", "\\\\\'").replaceAll("[\"]", "\\\\\"")
  }

  def getArticlesFromTags(tagStr: String): Seq[Article] = {
    val q = parseTags(tagStr).map(tag => s"${DB_TAGS}.name = '${escape(tag)}'").mkString(s" or ")
    val fieldNames = "posts.post_id as post_id, title, body, publish_date, update_date, likes" 
    val qry = s"select ${fieldNames} from ${DB_POSTS} inner join ${DB_TAGS} on ${DB_TAGS}.post_id = ${DB_POSTS}.post_id and (${q}) group by post_id"
    select[Post](qry).getOrElse(Seq()).map(post => new Article(post, getTags(post.postId), getComments(post.postId)))
  }

  def getArticleFromId(id: String): Option[Article] = {
    select[Post](s"select * from ${DB_POSTS} where post_id = ${id}").get.headOption match {
      case Some(p) => {
        Some(new Article(p.copy(body = convert(p.body)), getTags(p.postId), getComments(p.postId)))
      }
      case None => None 
    } 
  }

  def getIndexArticles(count: Int): Seq[Article] = {
    def getPosts(count: Int): Seq[Post] = select[Post](s"select * from ${DB_POSTS} order by publish_date desc limit ${count};").getOrElse(Seq())
    getPosts(count).map{ post => new Article(post, getTags(post.postId), getComments(post.postId)) }
  }

  def select[A](query: String)(implicit factory: DBEntityFactory[A]): Try[Seq[A]] = Try {
    val rs = getCon().createStatement.executeQuery(query)
    var seq = Seq[A]()
    while (rs.next()) { seq = seq :+ factory.create(rs) }
    seq
  }
}
