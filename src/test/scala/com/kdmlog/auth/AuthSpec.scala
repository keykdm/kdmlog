package com.kdmlog.auth

import org.scalatest._

class AuthSpec extends FunSuite {
  test("login should be success when correct user info is input.") {
    assert(Auth.authAccount(sys.env("API_USER"), sys.env("API_PASS")) == true)
  }

  test("login should be failed when incorrect user name is input.") {
    assert(Auth.authAccount("wrong", "pass") == false)
  }
  
  test("login should be failed when incorrect user pw is input.") {
    assert(Auth.authAccount("keikodama", "hoge") == false)
  }
}
