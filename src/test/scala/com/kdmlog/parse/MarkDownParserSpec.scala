package com.kdmlog.parse

import org.scalatest._
import scala.io.Source
import scala.util.Try

class MarkDownParserSpec extends FunSuite {
  val m = MarkDownParser
  val dataPath = "src/test/scala/com/kdmlog/data/"
  val className = getClass.getSimpleName
  val testDataPath = s"${dataPath}/${className}"

  def getData(fileName: String): String = Source.fromFile(s"${testDataPath}/${fileName}").getLines.mkString("\n")

  test("removeMarkDownSymbols should remove all .md symbols.") {
    assert(m.removeMarkDownSymbols("## Test ![test](http://url)") == " Test test(http://url)")
  }

  test("convert should convert title with ## into h2 tag") {
    assert(m.convert(getData("h2.data")) == getData("h2.expect"))
  }

  test("convert should convert title with # into h1 tag") {
    assert(m.convert(getData("h1.data")) == getData("h1.expect"))
  }

  test("convert should convert hyphenated list into ul tag list.") {
    assert(m.convert(getData("ul-hyphen.data")) == getData("ul-hyphen.expect"))
  }

  test("convert should convert astered list into ul tag list.") {
    assert(m.convert(getData("ul-aster.data")) == getData("ul-aster.expect"))
  }

  test("convert should convert plused list into ul tag list.") {
    assert(m.convert(getData("ul-plus.data")) == getData("ul-plus.expect"))
  }

  test("convert should convert ordered list into ol tag list.") {
    assert(m.convert(getData("ol-simple.data")) == getData("ol-simple.expect"))
  }

  test("convert should convert yaml like list into ul tag list.") {
    assert(m.convert(getData("yaml.data")) == getData("yaml.expect"))
  }

  test("convert should convert img md into img tag.") {
    def checkEnvs(): Try[Boolean] = Try {
      sys.env("DB_USER") == "kkodama" && sys.env("DB_PW") == "kkodamalog" && sys.env("GCLOUD_SQL_URL") == "172.17.0.2:3306/kdmlog_data"
    }

    if (checkEnvs().getOrElse(false)) {
      assert(m.convert(getData("img.data")) == getData("img.expect"))
    }
    else {
      println("DB test is not able to execute since environment variables are not defined.")
      true
    }  
  }

  test("convert should convet ```(type)code``` into code block.") {
    assert(m.convert(getData("code.data")) == getData("code.expect"))
  }
 
  test("convert should convert > hogehoge into blockquote.") {
    assert(m.convert(getData("blockquote.data")) == getData("blockquote.expect"))
  }
  
  test("convert should convert link into a tag.") {
    assert(m.convert(getData("link.data")) == getData("link.expect"))
  }
 
  test("convert should convert simple markdowned article into a html tagged article.") {
    assert(m.convert(getData("article.data")) == getData("article.expect"))
  }

  test("convert should convert inline element") {
    assert(m.convert(getData("inline.data")) == getData("inline.expect"))
  }

  test("convert should convert entire real article into htmled") {
    assert(m.convert(getData("entire.data")) == getData("entire.expect"))
  }
}
